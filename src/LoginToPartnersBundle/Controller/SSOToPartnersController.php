<?php

namespace LoginToPartnersBundle\Controller;

use Db\Entity\Beneficiaire;
use LoginToPartnersBundle\Services\SSOToPartnersService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;

class SSOToPartnersController
{
    /**
     * @Route("/sso-login/{partnerId}",
     *      name="connexion_to_partner")
     *
     * @Route("/{_locale}/sso-login/{partnerId}",
     *      name="connexion_to_partner_locale", requirements={"_locale" = "%allowed_languages%"})
     * @Security("is_granted('ROLE_CLIENT')")
     */
    public function sendASamlResponseFormToPartner(string $partnerId)
    {
        $languages = $this->get('AppBundle\Tools\Languages');
        $ssoToPartnersService = $this->get(SSOToPartnersService::class);
        $user = $this->getUser();

        if ($user instanceof Beneficiaire) {
            $ssoToPartnersService->buildHttpsPostFormForPartner($user, $partnerId);

            return $this->render('@LoginToPartners/formLoader.html.twig',
                [
                    'saml_response_form' => $ssoToPartnersService->getForm(),
                    'errors' => $ssoToPartnersService->getErrors(),
                ]
            );
        }

        return $this->redirectToRoute('login_page', ['_locale' => $languages->getCurrentLocal()]);
    }
}
