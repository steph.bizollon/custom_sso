<?php

namespace LoginToPartnersBundle\Services;

use LightSaml\Context\Profile\Helper\MessageContextHelper;
use LightSaml\Context\Profile\MessageContext;
use LightSaml\Credential\KeyHelper;
use LightSaml\Credential\X509Certificate;
use LightSaml\Helper;
use LightSaml\Model\Assertion\Assertion;
use LightSaml\Model\Assertion\Attribute;
use LightSaml\Model\Assertion\AttributeStatement;
use LightSaml\Model\Assertion\AudienceRestriction;
use LightSaml\Model\Assertion\AuthnContext;
use LightSaml\Model\Assertion\AuthnStatement;
use LightSaml\Model\Assertion\Conditions;
use LightSaml\Model\Assertion\Issuer;
use LightSaml\Model\Assertion\Subject;
use LightSaml\Model\Assertion\SubjectConfirmation;
use LightSaml\Model\Assertion\SubjectConfirmationData;
use LightSaml\Model\Protocol\Response;
use LightSaml\Model\Protocol\Status;
use LightSaml\Model\Protocol\StatusCode;
use LightSaml\Model\XmlDSig\SignatureWriter;
use LightSaml\SamlConstants;
use ExtApiBundle\Services\User;
use Db\Entity\Beneficiaire;
use BeneficiaireBundle\Exception\BeneficiaryNotFoundException;
use LoginToPartnersBundle\Exceptions\SsoPartnersException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\Translation\TranslatorInterface;

class SSOToPartnersService
{
    //in minutes
    const PRE_REQUEST_VALIDITY_TIME = 2;

    //in minutes
    const POST_REQUEST_VALIDITY_TIME = 2;

    const ISSUER = 'https://our-website-url.com';

    /** @var array sso partners config parameters */
    protected $ssoPartnersSp;

    /** @var string */
    protected $environment;

    /** @var TranslatorInterface */
    protected $translator;

    /** @var LoggerInterface */
    protected $logger;

    /** @var Response the LightSaml Response Object */
    private $xml;

    /** @var Assertion */
    private $assertion;

    /**
     * @var string
     */
    private $form;

    /**
     * @var array
     */
    private $errors;

    /**
     * @var User api user service
     */
    protected $apiUserService;

    /**
     * SSOToPartnersService constructor.
     *
     * @param                     $ssoPartnersSp
     * @param TranslatorInterface $translator
     * @param LoggerInterface     $logger
     * @param string              $environment
     */
    public function __construct(
        $ssoPartnersSp,
        TranslatorInterface $translator,
        LoggerInterface $logger,
        $environment,
        User $apiUserService
    ) {
        $this->ssoPartnersSp = $ssoPartnersSp;
        $this->translator = $translator;
        $this->logger = $logger;
        $this->xml = new Response();
        $this->assertion = new Assertion();
        $this->environment = $environment;
        $this->form = '';
        $this->errors = [];
        $this->apiUserService = $apiUserService;
    }

    /**
     * build the form we have to send by the http post method with Saml Response & Relay State in values.
     *
     * @param Beneficiaire $user
     * @param string       $partnerId
     */
    public function buildHttpsPostFormForPartner(Beneficiaire $user, string $partnerId)
    {
        try {
            if (key_exists($partnerId, $this->ssoPartnersSp)) {
                $partner = $this->ssoPartnersSp[$partnerId];
                $assertion = $this->buildSamlAssertion($user, $partnerId);

                if (!empty($assertion)) {
                    $this->form =
                        '<form method="post" action="'.$partner['url'].'" name="saml_response_form">'
                        .'<input type="hidden" name="SAMLResponse" value="'.$assertion.'" />';

                    if (!empty($partner['relay_state'])) {
                        $this->form = $this->form.'<input type="hidden" name="RelayState" value="'.$partner['relay_state'].'" />';
                    }
                    $this->form = $this->form.'</form>';
                } else {
                    throw new SsoPartnersException('There is a problem with the building of the assertion. Assertion is empty !');
                }
            } else {
                throw new SsoPartnersException('The partner Id : "'.$partnerId.'" doesn\'t exist. Please,'.' verify it is spelled correctly in the route and is well defined in "sso_partners_sp" parameters.yml');
            }
        } catch (SsoPartnersException $e) {
            $this->logger->warning('SSO Failed : '.$e->getMessage());

            $this->errors = [$this->translator->trans('fo.ssotopartners.partner-not-found', [], 'ssotopartners')];
        }
    }

    /**
     * Build the SamlAssertion xml and encode it for the http response.
     *
     * @param  Beneficiaire $user
     * @param  string       $partnerId
     * @return string
     */
    public function buildSamlAssertion(Beneficiaire $user, string $partnerId): string
    {
        try {
            $partner = $this->ssoPartnersSp[$partnerId];

            $userData = $this->getUserData($user, $partnerId);

            $this->generateXmlFirstPart($partner);
            $this->generateXmlAttributeAndAuthnStatement($userData, $partner);

            if ($this->signXML($partnerId)) {
                $samlAssertion = $this->serializeSamlAssertion();

                return base64_encode($samlAssertion);
            }
            throw new FileNotFoundException('Security Warning : Certificate or Private key not found.');
        } catch (FileNotFoundException $e) {
            $this->logger->critical('SAML Assertion not built : '.$e->getMessage());

            return '';
        } catch (BeneficiaryNotFoundException $e) {
            $this->logger->critical('SAML Assertion not built : '.$e->getMessage());

            return '';
        }
    }

    /**
     * Apply certificate and signatures to the XML for security.
     * @param $partnerId
     * @return bool
     */
    public function signXML($partnerId): bool
    {
        $certificateFile = $this->ssoPartnersSp[$partnerId]['certificate'];
        $privateKeyFile = $this->ssoPartnersSp[$partnerId]['private_key'];
        $x509FilePath = __DIR__ . '/../Resources/certificates/' .$certificateFile;
        $privateKeyFilePath = __DIR__ . '/../Resources/certificates/' .$privateKeyFile;
        $passphrase = $this->ssoPartnersSp[$partnerId]['passphrase'];

        if (file_exists($x509FilePath) && file_exists($privateKeyFilePath)) {
            $certificate = X509Certificate::fromFile($x509FilePath);
            $privateKey = KeyHelper::createPrivateKey($privateKeyFilePath, $passphrase, true);

            $this->xml->setSignature(new SignatureWriter($certificate, $privateKey));

            return true;
        }

        return false;
    }

    /**
     * @param array $partner the partner parameters
     */
    public function generateXmlFirstPart(array $partner): void
    {
        $this->xml
            ->addAssertion($this->assertion)
            ->setStatus(new Status(
                    new StatusCode(
                        SamlConstants::STATUS_SUCCESS)
                )
            )
            ->setID(Helper::generateID())
            ->setIssueInstant($this->getFormatedTimeWithDelay($partner)['currentDateTime'])
            ->setDestination($partner['url'])
            ->setIssuer(new Issuer(self::ISSUER)
            );

        $this->assertion
            ->setId(Helper::generateID())
            ->setIssueInstant($this->getFormatedTimeWithDelay($partner)['currentDateTime'])
            ->setIssuer(new Issuer(self::ISSUER))
            ->setSubject(
                (new Subject())
                    ->setNameID()
                    ->addSubjectConfirmation(
                        (new SubjectConfirmation())
                            ->setMethod(SamlConstants::CONFIRMATION_METHOD_BEARER)
                            ->setSubjectConfirmationData(
                                (new SubjectConfirmationData())
                                    ->setRecipient($partner['url'])
                            )
                    )
            )
            ->setConditions(
                ($conditions = new Conditions())
                    ->setNotBefore($this->getFormatedTimeWithDelay($partner)['startOfValidityDateTime'])
                    ->setNotOnOrAfter($this->getFormatedTimeWithDelay($partner)['endOfValidityDateTime'])
            );

        if (isset($partner['audience'])) {
            $conditions->addItem(
                new AudienceRestriction([$partner['audience']])
            );
        }
    }

    /**
     * @param array $userData
     * @param array $partner  the partner parameters
     */
    public function generateXmlAttributeAndAuthnStatement(array $userData, array $partner): void
    {
        foreach ($userData as $key => $value) {
            $this->assertion->addItem(
                (new AttributeStatement())
                    ->addAttribute(new Attribute(
                        $key,
                        $value
                    ))
            );
        }

        $this->assertion
            ->addItem(
                (new AuthnStatement())
                    ->setAuthnInstant($this->getFormatedTimeWithDelay($partner)['authInstant'])
                    ->setAuthnContext(
                        (new AuthnContext())
                            ->setAuthnContextClassRef(SamlConstants::AUTHN_CONTEXT_UNSPECIFIED)
                    )
            )
        ;
    }

    /**
     * get the xml from the LightSaml Response object.
     *
     * @return string
     */
    public function serializeSamlAssertion(): string
    {
        $messageContext = new MessageContext();
        $messageContext->setMessage($this->xml);

        $message = MessageContextHelper::asSamlMessage($messageContext);

        $serializationContext = $messageContext->getSerializationContext();
        $message->serialize($serializationContext->getDocument(), $serializationContext);
        $samlResponse = $serializationContext->getDocument()->saveXML();

        return $samlResponse;
    }

    /**
     * @param  array $partner
     * @return array all the timestamps we need with partner delay for assertion validity
     */
    public function getFormatedTimeWithDelay(array $partner): array
    {
        date_default_timezone_set('UTC');
        $millisecondZ = substr(microtime(false), 2, 6).'Z';
        $dayT = date('Y-m-d').'T';
        $formatedTime['currentDateTime'] = $dayT.date('H:i:s').'.'.$millisecondZ;
        $formatedTime['startOfValidityDateTime'] = $dayT.date('H:i:s', strtotime('now - '.self::PRE_REQUEST_VALIDITY_TIME.' minutes')).'.'.$millisecondZ;
        $formatedTime['endOfValidityDateTime'] = $dayT.date('H:i:s', strtotime('now + '.self::POST_REQUEST_VALIDITY_TIME.' minutes')).'.'.$millisecondZ;
        $formatedTime['authInstant'] = $dayT.date('H:i:s', strtotime('now - 10 minutes')).'.'.$millisecondZ;

        return $formatedTime;
    }

    /**
     * @param Beneficiaire $user
     * @param string       $partnerId
     *
     * @return array                                                      the user data the partner needs for the SSO authentification
     * @throws BeneficiaryNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws ExtApiBundle\Exception\ConnectionException
     * @throws ExtApiBundle\Exception\NotFoundUserException
     */
    public function getUserData(Beneficiaire $user, string $partnerId): array
    {
        $partner = $this->ssoPartnersSp[$partnerId];

        if ('DEV' === $this->environment || 'REC' === $this->environment) {
            $userData = $this->getTestUserData($partnerId);
        } else {
            $userByApi = $this->apiUserService->getBeneficiary($user);

            if (!empty($userByApi)) {
                $dob = (new \DateTime($userByApi->birthDate))->format('d/m/Y');
                if ('partner1' === $partnerId) {
                    $userData['NameId'] = $userByApi->importExtraFields->partner1_member_ref;
                    $userData['CompanyIdentificationType'] = $partner['company_identification_type'];
                    $userData['DOB'] = $dob;
                    $userData['Email'] = $userByApi->contact->email;
                    $userData['UserID'] = $userByApi->importExtraFields->partner1_member_ref;
                    $userData['ClientID'] = $partner['client_id'];
                    $userData['IdentificationType'] = $partner['identification_type'];
                } elseif ('partner2' === $partnerId) {
                    $userData['NameID'] = $userByApi->importExtraFields->partner2_name_id;
                    $userData['FirstName'] = $userByApi->firstName;
                    $userData['LastName'] = $userByApi->lastName;
                    $userData['Email'] = $userByApi->contact->email;
                    $userData['DOB'] = $dob;
                    $userData['CompanyName'] = $userByApi->client->name;
                    $userData['EmployeeId'] = $userByApi->importExtraFields->partner2_employee_id;
                }
            } else {
                throw new BeneficiaryNotFoundException('Returned beneficiary by API is empty');
            }
        }

        return $userData;
    }

    /**
     * @param string $partnerId
     *
     * @return array of the user data the partner needs for the SSO authentification
     */
    public function getTestUserData(string $partnerId): array
    {
        if ('partner1' === $partnerId) {
            // test data awaited in partner1 PPRD env
            $userData['NameId'] = '45784578';
            $userData['CompanyIdentificationType'] = '2';
            $userData['DOB'] = '01/01/1980';
            $userData['Email'] = 'email@mailbox.com';
            $userData['IdentificationType'] = '1';
            $userData['UserID'] = '45784578';
            $userData['ClientID'] = '45784578-45784578';
        } elseif ('partner2' === $partnerId) {
            //test data awaited in partner2 test env
            $userData['NameID'] = '123456789';
            $userData['FirstName'] = 'Jane';
            $userData['LastName'] = 'Doe';
            $userData['Email'] = 'email@mailbox.com';
            $userData['DOB'] = '01/01/1901';
            $userData['CompanyName'] = 'Company';
            $userData['EmployeeId'] = '987654321';
        }

        return $userData;
    }

    /**
     * @return string
     */
    public function getForm(): string
    {
        return $this->form;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
